# Q Client Execution Specs



## Description

Inspired by the [Ethereum Execution Specs](https://github.com/ethereum/execution-specs), but heavily reduced to get started.


## Ethereum Protocol Releases

| Version and Code Name | Block No. | Released | Specs | Blog |
|-----------------------|-----------|----------|-------|-------|
| Athos | TBD | TBD | [Specification](https://gitlab.com/q-dev/execution-specs/-/blob/main/network-upgrades/mainnet-upgrades/athos.adoc) | |
| Q Inception | 1 | 2022-03-22 | | [Blog](https://medium.com/q-blockchain/q-mainnet-is-live-b47682cd2e8a) |
