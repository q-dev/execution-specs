# Athos Hard Fork

[cols="1,1"]
|===
| Creation Date | 2022-10-06 
| Latest Update | 2022-11-25
| Contributors  | @TLatzke
| Status        | Final
|===

link:https://en.wikipedia.org/wiki/Mount_Athos[Mount Athos]

## Summary

Athos will be the first hard fork of the Q blockchain.
It contains the important mechanism of account aliases which improves the operational security for validators and root nodes.
Closely related is the possibility of dedicated withdrawal addresses in the respective staking contracts.
Minor but still consensus relevant changes have been added for robustness and future proofness.
The alias feature requires a constitution update.

## Content

NOTE: All provided links are permalinks to specific revisions. This allows for revision proof voting on the proposal.

* https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00002.adoc[QIP-00002 - Activate Berlin Hard Fork]
* https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00003.adoc[QIP-00003 - Account Aliases]
* https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00004.adoc[QIP-00004 - Withdraw Addresses]
* https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00005.adoc[QIP-00005 - Unban Validators]
* https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00006.adoc[QIP-00006 - Increase Byte Code Limit]

### Constitution

https://gitlab.com/q-dev/QIPs/-/blob/463d59c24b890b7bf569be52e0480a584217cbda/QIPs/QIP-00003.adoc[QIP-00003 - Account Aliases] requires a constitution update.

The proposed link:./25a0ad3b8de911b1745691b83875291c75f424487551ec83f7ef9ae8205ef059.adoc[new version] leads to the new constitution hash `25a0ad3b8de911b1745691b83875291c75f424487551ec83f7ef9ae8205ef059`.

The changes affect (among others) the chapters 1-3 and their definitions. Therfore this is considered a **fundamental change**.

## Activation Blocks

[cols="1,1"]
|===
| Network | Activation Block

| Fischer Testnet | `3_459_000` (ETA 2022-11-23)
| Q Mainnet       | `5_075_000` (ETA 2023-01-11)
|===

### Implementation condition

If by the time of the activation block, no suitable Q-client implementation exists, this upgrade can be postponed by 518,000 blocks (~30 days).

If by the time of the postponed activation block, the issue is still not solved, the upgrade shall not be activated (unless there is a new valid vote in accordance with the Q Constitution) .

A Q-client implementation is considered suitable in this context if

* there is no evidence that it does not properly implement one of the included QIPs **and** a fix to the proper implementation would require another hard fork
* there is no evidence that it introduces a material harm to the network, which does not exist in the current implementation

For the avoidance of doubt, this provision is included to provide emergency protection only.
The activation should only be postponed if there is no Q-client implementation implementing the upgrade,
or if there is clear evidence (e.g. through public communication by the respective Q-client’s developers) that the Q-client implementation is not suitable.
In the absence of such clear evidence, it is assumed that the Q-client implementation is suitable, and the activation block shall remain unchanged.
